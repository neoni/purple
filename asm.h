#ifndef DISASM_H
#define DISASM_H

#include "func.h"

int disasm(Func *func);
int asmble(char *code, char *bin, size_t *size, char *filename);
void cal_bl(size_t src, size_t dst, size_t *bin);
int cal_blx(size_t dst, char *bin);
void cal_bne(size_t src, size_t dst, size_t *bin);
int extract_op_mem(cs_insn *insn, size_t *reg, size_t *imm);

#endif
