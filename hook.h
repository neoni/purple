#ifndef HOOK_H
#define HOOK_H


#define PTMX_DEVICE      "/dev/ptmx"
#define OFF_FSYNC        0x38
#define OFF_IOCTL        0x20


int set_func_pointer(size_t addr, size_t *ori_addr, size_t offset);
int apply_func(char *func);


#endif
