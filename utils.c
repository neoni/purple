#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sched.h>

#include "utils.h"
#include "locate.h"
#include "asm.h"
#include "prot.h"


int get_version() {
/*  read kernel verion
    int fd;
    char s[256] = {0};

    fd = open("/proc/version", O_RDONLY);
    if (fd < 0) {
        printf("read version failed\n");
        return NULL;
    }
    read(fd, s, sizeof(s));
#ifdef DEBTAIL
    printf("read version: %s\n", s);
#endif
    strtok(s, " ");
    strtok(NULL, " ");
    version = strtok(NULL, "-");
    version[strlen(version)] = '\x00';

    close(fd);
*/
    FILE *fp;
    char line[100];

    fp = fopen("/system/build.prop", "r");
    if (fp == NULL) {
        printf("read version failed\n");
        return -1;
    }

    while (fgets(line, sizeof(line), fp)) {
        if (strncmp(line, "ro.build.version.release=", 25) == 0) {
            char v[100];
            strncpy(v, line+25, strlen(line)-25);
            v[strlen(line)-25] = '\x00';
#ifdef DEBUG
    printf("os version: %s\n", v);
#endif
            if (strncmp(v, "4", 1) == 0) {
                return 4;
            } else {
                return 5;
            }
        }
    }

    fclose(fp);
    return -1;
}

void startup(char *cvename) {

    printf("\n*******************************************\n");
    printf("Start patching %s...\n", cvename);
}


int vmalloc_exe_kmem(size_t *patch_addr) {
    size_t pmd_val = 0;
    size_t vmalloc_addr, syscall_addr;
    char ori_code[100], code[100], cmd[150];

    if (get_symbol_addr(SYSCALL_R, &syscall_addr) == -1) {
        return -1;
    }

    if (get_symbol_addr(VMALLOC_EXE, &vmalloc_addr) == -1) {
        return -1;
    }

    if (change_page_perm(swapper_pg_dir, syscall_addr, &pmd_val, false, PERM_W) == -1) {
        return -1;
    }


    char bl_bin[10];
    cal_bl(syscall_addr+12, vmalloc_addr, (size_t *)bl_bin);
    bl_bin[4] = '\x00';

    memcpy(code, VM_CODE_BIN_I, 12);
    memcpy(code+12, bl_bin, 4);
    memcpy(code+16, VM_CODE_BIN_II, 4);
    code[20] = '\x00';

#ifdef DEBUG
    size_t i;
    printf("syscall: 0x%08x, vmalloc: 0x%08x\n", syscall_addr, vmalloc_addr);
    for (i = 0; i < VM_CODE_SIZE; ++i) {
        printf("%02x", code[i]);
    }
    printf("\n");
#endif

    read_kmem(syscall_addr, ori_code, VM_CODE_SIZE);
#ifdef DETAIL
    for (i = 0; i < VM_CODE_SIZE; ++i) {
        printf("%02x", ori_code[i]);
    }
    printf("\n");
#endif

#ifdef DETAIL
    char c[100];
    printf("origin: ");
    read_kmem(syscall_addr, c, VM_CODE_SIZE);
    for (i = 0; i < VM_CODE_SIZE; ++i) {
        printf("%02x", c[i]);
    }
    printf("\n");
#endif

    snprintf(cmd, 150, "./write_ktext %d %d", syscall_addr, VM_CODE_SIZE);
#ifdef DETAIL
    printf("cmd: %s\n", cmd);
#endif
    if (write_ktext(code, VM_CODE_SIZE) == -1) {
        return -1;
    }
    if (system(cmd) == -1) {
        return -1;
    }


    *patch_addr = 0;
    *patch_addr = sched_yield();

    printf("Generate patch addr is 0x%08x\n", *patch_addr);

#ifdef DETAIL
    printf("modified: ");
    read_kmem(syscall_addr, c, VM_CODE_SIZE);
    for (i = 0; i < VM_CODE_SIZE; ++i) {
        printf("%02x", c[i]);
    }
    printf("\n");
#endif

    snprintf(cmd, 150, "./write_ktext %d %d", syscall_addr, VM_CODE_SIZE);
#ifdef DETAIL
    printf("cmd: %s\n", cmd);
#endif
    if (write_ktext(ori_code, VM_CODE_SIZE) == -1) {
        return -1;
    }
    if (system(cmd) == -1) {
        return -1;
    }
#ifdef DETAIL
    printf("restored: ");
    read_kmem(syscall_addr, c, VM_CODE_SIZE);
    for (i = 0; i < VM_CODE_SIZE; ++i) {
        printf("%02x", c[i]);
    }
    printf("\n");
#endif

    if (change_page_perm(swapper_pg_dir, syscall_addr, &pmd_val, true, PERM_W) == -1) {
        return -1;
    }

    return 0;
}
