#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "func.h"
#include "kmem.h"
#include "locate.h"
#include "asm.h"

static void _init_func(Func *func, char* name) {
    strncpy(func->name, name, strlen(name));
    func->name[strlen(name)] = '\x00';
    func->addr = 0;
    func->size = 0;
    func->insn_count = 0;
    func->insn = NULL;
    func->code = NULL;
    func->handle = 0;
    func->next = NULL;
}

int init_func(Func *func, char* name, bool fuzz) {

    _init_func(func, name);

    if (locate_by_symbol(func, fuzz) == -1) {
        return -1;
    }

    func->code = malloc(func->size + 1);

    if (read_kmem_func(func) == -1) {
        return -1;
    }

#ifdef DETAIL
    for (i = 0; i < func->size; ++i) {
        printf("%02x", *(uint8_t *)((uint8_t *)func->code+i));
    }
    printf("\n");
#endif

    if (disasm(func) == -1) {
        return -1;
    }

    return 0;
}

void fini_func(Func *func) {

    if (func->code) {
        free(func->code);
    }

    if (func->insn) {
        cs_free(func->insn, func->insn_count);
    }

    if (func->handle) {
        cs_close(&func->handle);
    }
}
