#ifndef PGTABLE_H
#define PGTABLE_H

#include "kmem.h"

typedef signed int s32;
typedef unsigned int u32;
typedef signed long long s64;
typedef unsigned long long u64;

typedef u32 pteval_t;
typedef u32 pmdval_t;
typedef u32 phys_addr_t;

typedef pteval_t pte_t;
typedef pmdval_t pmd_t;
typedef u64 pgd_t;
typedef struct { pgd_t pgd; } pud_t;

#define pte_val(x)      (x)
#define pmd_val(x)      (x)
#define pgd_val(x)      (x)

#define __ALIGN_KERNEL_MASK(x, mask)    (((x) + (mask)) & ~(mask))
#define __ALIGN_KERNEL(x, a)        __ALIGN_KERNEL_MASK(x, (typeof(x))(a) - 1)
#define ALIGN(x, a)                 __ALIGN_KERNEL((x), (a))
#define PAGE_ALIGN(addr)            ALIGN(addr, PAGE_SIZE)


#define SECTION_SHIFT               20
#define SECTION_SIZE                (1UL << SECTION_SHIFT)

#define SWAPPER_PG_DIR              0xC0004000
#define PHYS_MASK                   (~0UL)

#define PTRS_PER_PTE                512
#define PTRS_PER_PMD                1
#define PTRS_PER_PGD                2048
#define PMD_SHIFT                   21
#define PGDIR_SHIFT                 21
#define PMD_SIZE                    (1UL << PMD_SHIFT)
#define PMD_MASK                    (~(PMD_SIZE-1))
#define PGDIR_SIZE                  (1UL << PGDIR_SHIFT)
#define PGDIR_MASK                  (~(PGDIR_SIZE-1))

static inline pmd_t *pmd_offset(pud_t *pud, unsigned long addr) {
    return (pmd_t *)pud;
}


#define PAGE_OFFSET                 0xC0000000
#define PHYS_OFFSET                 0x00000000
#define __virt_to_phys(x)           ((x) - PAGE_OFFSET + PHYS_OFFSET)
#define __phys_to_virt(x)           ((x) - PHYS_OFFSET + PAGE_OFFSET)
#define __va(x)                     ((void *)__phys_to_virt((unsigned long)(x)))

static inline size_t read_pmd_val(pmd_t *pmd) {
    size_t buf;
    read_kmem((size_t)pmd, &buf, 4);
    return buf;
}

static inline pte_t *pmd_page_vaddr(pmd_t *pmd) {
    return __va(read_pmd_val(pmd) & PHYS_MASK & (s32)PAGE_MASK);
}


#define pud_val(x)                  (pgd_val((x).pgd))

#define swapper_pg_dir              ((pgd_t *) SWAPPER_PG_DIR)
#define pgd_index(addr)             ((addr) >> PGDIR_SHIFT)
#define pgd_offset_k(x, addr)       (x + pgd_index(addr))

#define PMD_TYPE_MASK               (_AT(pmdval_t, 3) << 0)

#define pte_index(addr)             (((addr) >> PAGE_SHIFT) & (PTRS_PER_PTE - 1))
#define pte_offset_kernel(pmd,addr) (pmd_page_vaddr(pmd) + pte_index(addr))


#define _AT(T,X)                    ((T)(X))
#define PMD_TYPE_TABLE              (_AT(pmdval_t, 1) << 0)
#define PMD_TYPE_SECT               (_AT(pmdval_t, 2) << 0)
#define PMD_SECT_APX                (_AT(pmdval_t, 1) << 15)
#define PMD_SECT_XN                 (_AT(pmdval_t, 1) << 4)
#define PMD_SECT_PXN                (_AT(pmdval_t, 1) << 0)


#define PERM_W                      4
#define PERM_X                      1


#define PGD_START                   0xe7000000
#define PGD_END                     0xed000000
#define KTEXT_START                 0xc0008000


#define CODE_FILE                   "code.bin"


int change_page_perm(pgd_t *pgd, size_t addr, size_t *pmd_saved, bool restore, size_t perm);
int write_kernel_text(size_t addr, char *code, size_t code_size, size_t perm);
int write_ktext(char *code, size_t code_size);

#endif
