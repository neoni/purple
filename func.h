#ifndef FUNCTION_H
#define FUNCTION_H

#include <capstone.h>

typedef struct _Func {
    char name[51];
    size_t addr;
    size_t size;
    size_t insn_count;
    void *code;
    cs_insn *insn;
    csh handle;
    struct _Func *next;
} Func;


int init_func(Func *func, char* name, bool fuzz);
void fini_func(Func *func);

#endif
