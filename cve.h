#ifndef CVE_H
#define CVE_H

#include "patch.h"
#include "func.h"

int locate_cve_2015_3636(Patch *patch);
int locate_cve_2014_3153(Patch *patch);

const static struct {
  const char *name;
  int (*func)(Patch *patch);
} function_map [] = {
  { "cve_2015_3636", locate_cve_2015_3636 },
  { "cve_2014_3153", locate_cve_2014_3153 },
};

int call_func(Patch *patch);

#endif
