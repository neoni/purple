#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>
#include <errno.h>


#include "pfn.h"
#include "kmem.h"
#include "locate.h"


#ifdef BLAST
int fill_with_magic(void *address, size_t size) {
    unsigned *p = address;
    int i;

    for (i = 0; i < size; i += sizeof (*p)) {
        *p = MAGIC;
        p++;
    }
    return 0;
}

int find_pfn_min(size_t pfn, size_t *pfn_min) {
    size_t min_lpfn_addr, max_lpfn_addr;
    size_t min_lpfn, max_lpfn;


    if (get_symbol_addr("min_low_pfn", &min_lpfn_addr) == -1) {
        return -1;
    }
    if (read_kmem(min_lpfn_addr, &min_lpfn, sizeof(min_lpfn)) == -1) {
        return -1;
    }
#ifdef DETAIL
    printf("min_low_pfn is 0x%x\n", min_lpfn);
#endif

    if (get_symbol_addr("max_low_pfn", &max_lpfn_addr) == -1) {
        return -1;
    }
    if (read_kmem(max_lpfn_addr, &max_lpfn, sizeof(max_lpfn)) == -1) {
        return -1;
    }
#ifdef DETAIL
    printf("max_low_pfn is 0x%x\n", max_lpfn);
#endif

    size_t p;
    size_t vaddr;
    for (p = min_lpfn; p <= max_lpfn; ++p) {
        vaddr = PHYSMAP_OFFSET + PFN_PAGE_SIZE * (pfn - p);
        if (vaddr >= PHYSMAP_OFFSET && vaddr <= PHYSMAP_END) {
            size_t buf = 0;
            if (read_kmem(vaddr, &buf, 4) != -1) {
                if (buf == MAGIC) {
                    printf("vaddr is 0x%x, pfn_min is 0x%x\n", vaddr, p);
                    printf("buf is 0x%x\n", buf);
                }
            }
        }

    }

    return 0;
}


void search_phys() {
    size_t buf;
    size_t addr = PHYSMAP_OFFSET;
    do {
        read_kmem(addr, &buf, 4);
        if (buf == MAGIC) {
            printf("Found: addr is 0x%x\n", addr);
            break;
        }
        addr += 4;
    } while (addr < PHYSMAP_END);
}
#endif


static inline int get_pfn(size_t vaddr, size_t *pfn) {
    size_t psize;

    /* pagemap entry */
    uint64_t    pentry  = 0;
    /* pagemap file */
    FILE        *fp = NULL;

    /* get the page size */
    if ((psize = sysconf(_SC_PAGESIZE)) == -1) {
        printf("failed while trying to read page size -- %s\n", strerror(errno));
        return -1;
    }

    if ((vaddr & (psize - 1)) != 0) {
        printf("virtual address %#x is not page-aligned; converting to %#lx\n",
              vaddr, vaddr & (ULONG_MAX - (psize - 1)));
        /* fix the virtual address */
        vaddr &= ULONG_MAX - (psize - 1);
    }

    if ((fp = fopen("/proc/self/pagemap", "r")) == NULL) {
        printf("failed while trying to open pagemap -- %s\n", strerror(errno));
        return -1;
    }

    if (fseek(fp, (vaddr / psize) * sizeof(uint64_t), SEEK_CUR) == -1) {
        printf("failed while trying to seek in pagemap -- %s\n", strerror(errno));
        fclose(fp);
        return -1;
    }

    /* read the corresponding pagemap entry */
    if (fread(&pentry, sizeof(uint64_t), 1, fp) != 1) {
        if (ferror(fp)) {
            printf("failed while trying to read a pagemap entry -- %s\n", strerror(errno));
        }
        else {
            printf("unknown error while trying to read a pagemap entry -- %s\n", strerror(errno));
        }
        fclose(fp);
        return -1;
    }

#ifdef DETAIL
    printf("pentry is %llx\n", pentry);
#endif

    /* check the present bit */
    if ((pentry & PRESENT_MASK) == 0) {
        printf("%#x is not present in physical memory\n", vaddr);
        fclose(fp);
        return -1;
    }
    else {
#ifdef DEBUG
        printf("PFN[%#x]: 0x%llx\n", vaddr, pentry & PFN_MASK);
#endif
        *pfn = pentry & PFN_MASK;
    }

    fclose(fp);

    return 0;
}


static inline int lock_page_in_memory(void *address, size_t size) {
  int ret;

  ret = mlock(address, size);
  if (ret != 0) {
      return -1;
    }

  return 0;
}


static int get_physmap_pfn(size_t *pfn) {

    static void *mmap_addr[MAX_MMAPS];
    size_t max_pfn, max_pfn_addr;
    int i;
    int success = 0;

    if (get_symbol_addr("max_low_pfn", &max_pfn_addr) == -1) {
        return -1;
    }
    if (read_kmem(max_pfn_addr, &max_pfn, 4) == -1) {
        return -1;
    }
#ifdef DEBUG
    printf("max_low_pfn is 0x%x\n", max_pfn);
#endif

    for (i = 0; i < MAX_MMAPS; i++) {

        mmap_addr[i] = mmap(NULL, MMAP_SIZE,
                        PROT_READ | PROT_WRITE | PROT_EXEC,
                        MAP_SHARED | MAP_ANONYMOUS,
                        -1, 0);

        if (mmap_addr[i] == MAP_FAILED) {
            printf("patch addr mmap failed\n");
            break;
        }

        if (lock_page_in_memory(mmap_addr[i], MMAP_SIZE) == -1) {
            printf("mlock failed\n");
            break;
        }

        get_pfn((size_t)mmap_addr[i], pfn);
        if (*pfn <= max_pfn) {
#ifdef BLAST
            fill_with_magic(mmap_addr[i], MMAP_SIZE);
#endif
            //final_mmap_addr = mmap_addr[i];
            mmap_addr[i] = 0;
            success = 1;
            printf("valid pfn is 0x%x\n", *pfn);
            break;
        }
    }

    int count = i;
    for (i = 0; i < count; ++i) {
        if (mmap_addr[i]) {
            munmap(mmap_addr[i], MMAP_SIZE);
        }
    }

    if (success)
        return 0;

    return -1;
}


int gen_patchaddr(size_t *patch_addr, size_t insn_addr) {
    size_t pfn_suc = 1;

    while (pfn_suc) {
        size_t pfn;
        if (get_physmap_pfn(&pfn) == -1) {
            return -1;
        }

        size_t pfn_min = PFN_MIN;
#ifdef BLAST
        find_pfn_min(pfn, &pfn_min);
#endif

        *patch_addr = PHYSMAP_OFFSET + PFN_PAGE_SIZE * (pfn - pfn_min);
        if (*patch_addr <= PHYSMAP_LIMIT && *patch_addr <= insn_addr + BL_MAX) {
            pfn_suc = 0;
        }
    }

    printf("patch_addr is 0x%x\n", *patch_addr);

    return 0;
}
