#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#define CODE_FILE     "code.bin"

int write_kmem(size_t offset, void *buf, size_t count) {
    int fd;
    int n = 0;

    fd = open("/dev/kmem", O_WRONLY);
    if (fd < 0) {
        perror("open /dev/kmem failed");
        return -1;
    }

    if (lseek(fd, offset, SEEK_SET) != offset) {
        perror("/dev/kmem lseek failed\n");
        close(fd);
        return -1;
    }

    while (n < count) {
        int res = write(fd, buf+n, count-n);
        if (res < 0) {
            perror("/dev/kmem write failed\n");
            close(fd);
            return -1;
        }
        n += res;
    }

    close(fd);
    return n;
}


int main(int argc, char** argv) {

    if (argc != 3) {
        printf("usage: ./write_ktext addr size\n");
        return 1;
    }

    size_t addr = atoi(argv[1]);
    size_t size = atoi(argv[2]);
    FILE* fp;
    char code[100];
    if ((fp = fopen(CODE_FILE, "rb")) == NULL) {
        perror("ERROR: Failed to open code.bin\n");
        return -1;
    }

    fread(code, 1, size, fp);
    fclose(fp);

    size_t i;
    for (i = 0; i < size; ++i) {
        //printf("%02x", code[i]);
        write_kmem(addr+i, code+i, 1);
    }
    printf("\n");

    //write_kmem(addr, code, strlen(code));

    return 0;
}
