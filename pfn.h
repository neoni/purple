#ifndef PFN_H
#define PFN_H


#define PRESENT_MASK                        (1ULL << 63)    /* get bit 63 from a 64-bit integer */
#define PFN_MASK                            ((1ULL << 55) - 1)  /* get bits 0-54 from 64-bit integer */
#define MMAP_SIZE                           (2 * 1024 * 1024)
#define MAX_MMAPS                           1024
#define PHYSMAP_OFFSET                      0xC0000000
#define PHYSMAP_END                         (PHYSMAP_OFFSET + 760 * 1024 * 1024)
#define PHYSMAP_LIMIT                       (PHYSMAP_OFFSET + 0xff00000)
#define BL_MAX                              0x2000000
#define PFN_PAGE_SIZE                       4096
#define PFN_MIN                             0
#define MAGIC                               0xDEADBEEF


int gen_patchaddr(size_t *patch_addr, size_t insn_addr);

#ifdef BLAST
int fill_with_magic(void *address, size_t size);
int find_pfn_min(size_t pfn, size_t *pfn_min);
void search_phys();
#endif


#endif
