OBJS :=  prot.o patch.o cve.o utils.o func.o kmem.o locate.o asm.o main.o
SYSROOT := /Users/neoni/Library/Android/android-ndk-r10e/platforms/android-19/arch-arm
LIBCINC := ./capstone/include
LIBKINC := ./keystone/include
CFLAGS := -Wall --sysroot=$(SYSROOT) -fPIC -I$(LIBCINC)
LDFLAGS := -static
LIBCAPSTONE := libcapstone.a
LIBKEYSTONE := libkeystone.a

TARGET := purple
CC := gcc-arm

.PHONY: clean all debug detail blast low

all: $(TARGET)

debug: CFLAGS += -DDEBUG
debug: $(TARGET)

test: CFLAGS += -DDEBUG -DTEST
test: $(TARGET)

detail: CFLAGS += -DDEBUG -DDETAIL
detail: $(TARGET)

blast: CFLAGS += -DDEBUG -DBLAST
blast: $(TARGET)

low: CFLAGS += -DDEBUG -DLOW_DEVICES
low: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(TARGET) $(OBJS) $(LIBCAPSTONE)


main.o: patch.h utils.h func.h kmem.h locate.h main.c libcapstone.a
func.o: func.h func.c
kmem.o: kmem.h kmem.c
locate.o: locate.h locate.c
asm.o: asm.h asm.c libcapstone.a
utils.o: utils.h utils.c
patch.o: patch.h patch.c
prot.o: prot.h prot.c
cve.o: cve.h cve.c

clean:
	-rm $(TARGET) $(OBJS)
