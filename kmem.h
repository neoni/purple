#ifndef KMEM_H
#define KMEM_H

#include "func.h"


int read_kmem(size_t offset, void *buf, size_t count);
int read_kmem_func(Func *func);
int write_kmem(size_t offset, void *buf, size_t count);


#endif
