#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <linux/types.h>
#include <linux/mm.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <linux/in.h>
#include <fcntl.h>
#include <errno.h>

#include "prot.h"
#include "locate.h"

static inline pud_t *pud_offset(pgd_t *pgd, unsigned long address) {
    return (pud_t *)pgd;
}

static inline pmd_t *pmd_off_k(pgd_t *pgd, unsigned long virt) {
    return pmd_offset(pud_offset(pgd_offset_k(pgd, virt), virt), virt);
}

static inline size_t read_pte_val(pte_t *pte) {
    size_t buf;
    read_kmem((size_t)pte, &buf, 4);
    return buf;
}


static int check_perm(size_t perm) {
    switch (perm) {
        case PERM_W: return ~PMD_SECT_APX;
        case PERM_X: return ~PMD_SECT_XN;
        default:
            return 0;
    }
    return 0;
}


int change_page_perm(pgd_t *pgd, size_t addr, size_t *pmd_saved, bool restore, size_t perm) {
    pmd_t *pmd;
    size_t pmd_val;
    size_t pmd_val_change;

    pmd = pmd_off_k(pgd, addr);
    if (addr & SECTION_SIZE) {
        pmd++;
    }
    pmd_val = read_pmd_val(pmd);

#ifdef DEBUG
    printf("%s: pmd %p->%08x for address %08x\n", __func__, pmd, pmd_val, addr);
#endif

    // section mapping
    if ((pmd_val & PMD_TYPE_MASK) == PMD_TYPE_SECT) {
        if (restore) {
            pmd_val_change = *pmd_saved;
        } else {
            size_t permission = check_perm(perm);
            if (permission == 0) {
                printf("wrong permission!\n");
                return -1;
            }
            pmd_val_change = pmd_val & (~PMD_SECT_PXN);
            pmd_val_change = pmd_val & permission;
        }
        *pmd_saved = pmd_val;
        write_kmem((size_t)pmd, &pmd_val_change, sizeof(pmd_val_change));

#ifdef DEBUG
        printf("change pmd(%p): %08x -> %08x\n", pmd, pmd_val, pmd_val_change);
#endif

    } else if ((pmd_val & PMD_TYPE_MASK) == PMD_TYPE_TABLE) {  // page mapping
        pte_t *pte;
        pte = pte_offset_kernel(pmd, addr);
        printf("pte: %08x = %08x\n", (size_t)pte, read_pte_val(pte));
        // todo
    } else {
        printf("bad addr mapping: pmd %p->%08x for address %08x\n", pmd, pmd_val, addr);
        return -1;
    }

    return 0;
}


// Notice: special chars '\x00' '\n' are also needed to be passed
int write_ktext(char *code, size_t code_size) {
    FILE *fp;

    fp = fopen(CODE_FILE, "wb");
    if (fp == NULL) {
        printf("write code bin failed\n");
        return -1;
    }
    fwrite(code, 1, code_size, fp);
    fclose(fp);

    return 0;
}

// use code_size, not strlen
int write_kernel_text(size_t addr, char *code, size_t code_size, size_t perm) {

    size_t pmd_val;

    if (change_page_perm(swapper_pg_dir, addr, &pmd_val, false, perm) == -1) {
        return -1;
    }


#ifdef DEBUG
    size_t buf;
    read_kmem((size_t)addr, &buf, 4);
    printf("%x->%08x\n", addr, buf);
#endif

    char cmd[100];

    if (write_ktext(code, code_size) == -1) {
        return -1;
    }
    snprintf(cmd, 100, "./write_ktext %d %d", addr, code_size);

#ifdef DEBUG
    printf("cmd: %s\n", cmd);
#endif
    if (system(cmd) != 0) {
        return -1;
    }

#ifdef DEBUG
    read_kmem(addr, &buf, 4);
    printf("%x->%08x\n", addr, buf);
#endif

    if (change_page_perm(swapper_pg_dir, addr, &pmd_val, true, perm) == -1) {
        return -1;
    }

    return 0;
}


