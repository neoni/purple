#include <stdio.h>
#include <stdlib.h>

#include "kmem.h"
#include "locate.h"

void init_symbol_maps() {
    system("echo 0 > /proc/sys/kernel/kptr_restrict sysctl");
    system("cat /proc/kallsyms > maps");
    system("echo 2 > /proc/sys/kernel/kptr_restrict sysctl");
}

int locate_by_symbol(Func *func, bool fuzz) {

    FILE *fp;
    char addr_str[11] = "0x", next_addr_str[11] = "0x";
    char var[51];
    size_t next_addr;
    char ch;
    int r;

    if ((fp = fopen("./maps", "r")) == NULL) {
            perror("fopen maps failed\n");
            return -1;
    }

    do {
            r = fscanf(fp, "%8s %c %50s\n", &addr_str[2], &ch, var);    // format of System.map
            if (fuzz) {
                if (strncmp(var, func->name, strlen(func->name)) == 0) {
                    r = fscanf(fp, "%8s %c %50s\n", &next_addr_str[2], &ch, var);    // format of System.map
                    break;
                }

            } else {
                if (strcmp(var, func->name) == 0) {
                    r = fscanf(fp, "%8s %c %50s\n", &next_addr_str[2], &ch, var);    // format of System.map
                    break;
                }
            }
    } while (r > 0);

    fclose(fp);

    if (r < 0) {
            printf("could not find symbol %s\n", func->name);
            return -1;
    }

    func->addr = strtoul(addr_str, NULL, 16);                               //Convert string to unsigned long integer
    next_addr = strtoul(next_addr_str, NULL, 16);
    func->size = (long)(next_addr - func->addr);
#ifdef DEBUG
    printf("found symbol %s at 0x%08x, size: 0x%08x\n", func->name, func->addr, func->size);
#endif

    return 0;
}


int get_symbol_addr(char *name, size_t *addr) {

    FILE *fp;
    char addr_str[11] = "0x";
    char var[51];
    char ch;
    int r;

    if ((fp = fopen("./maps", "r")) == NULL) {
            perror("fopen maps failed\n");
            return -1;
    }

    do {
            r = fscanf(fp, "%8s %c %50s\n", &addr_str[2], &ch, var);    // format of System.map
            if (strcmp(var, name) == 0) {
                break;
            }
    } while (r > 0);

    fclose(fp);

    if (r < 0) {
            printf("could not find symbol %s\n", name);
            return -1;
    }

    *addr = strtoul(addr_str, NULL, 16);                               //Convert string to unsigned long integer
#ifdef DETAIL
    printf("found symbol %s at 0x%08x\n", name, *addr);
#endif

    return 0;
}


