#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "patch.h"
#include "asm.h"
#include "kmem.h"
#include "prot.h"

void init_patch(Patch *patch, char* name) {
    strncpy(patch->name, name, strlen(name));
    patch->name[strlen(name)] = '\x00';
    patch->patchinsn = NULL;
    patch->func = NULL;
}


void fini_patch(Patch *patch) {
    PatchInsn *insn = patch->patchinsn;
    PatchInsn *nextinsn;
    while (insn) {
        nextinsn = insn->next;
        free(insn);
        insn = nextinsn;
    }

    Func *func = patch->func;
    Func *nextfunc;
    while (func) {
        nextfunc = func->next;
        fini_func(func);
        free(func);
        func = nextfunc;
    }
}


void add_patchinsn(Patch *patch, size_t addr, size_t size, char *code) {

    PatchInsn *patchinsn = patch->patchinsn;
    PatchInsn *insn = malloc(sizeof(PatchInsn));

    insn->insn_addr = addr;
    insn->patch_size = size;
    memcpy(insn->patch_code, code, size);
    insn->patch_code[size] = '\x00';
    insn->next = NULL;

    if (patchinsn == NULL) {
        patch->patchinsn = insn;
    } else {
        while (patchinsn->next) {
            patchinsn = patchinsn->next;
        }
        patchinsn->next = insn;
    }
}


int nop_insn(size_t addr, size_t insn_num) {

    char bin[insn_num*4];
    size_t i;
    for (i = 0; i < insn_num*4; i+=4) {
        memcpy(bin+i, NOP, 4);
    }

    if (write_kernel_text(addr, bin, insn_num*4, PERM_W) == -1) {
        printf("write kernel text failed\n");
        return -1;
    }

    return 0;
}



int start_patch(Patch *patch, size_t *patch_addr) {

    PatchInsn *insn = patch->patchinsn;

    while (insn) {

        char ori_bin[13], patch_bin[300];
        read_kmem(insn->insn_addr, ori_bin, 12);
        ori_bin[12] = '\x00';
        memcpy(patch_bin, insn->patch_code, insn->patch_size);
        memcpy(patch_bin+insn->patch_size, ori_bin, 12);
        memcpy(patch_bin+insn->patch_size+12, PATCH_CODE_END, 4);
        insn->patch_size += 16;
        patch_bin[insn->patch_size] = '\x00';

#ifdef DEBUG
        printf("patch bin:\n");
        size_t i;
        for (i = 0; i < insn->patch_size; ++i) {
            printf("%02x", patch_bin[i]);
        }
        printf("\n");
#endif

        if (write_kmem(*patch_addr, patch_bin, insn->patch_size) == -1) {
            printf("Writing patching code failed\n");
            return -1;
        }

#ifdef DEBUG
        size_t buf;
        read_kmem(*patch_addr, &buf, 4);
        printf("patch code(4): %08x\n\n", buf);
#endif
        // patching: add jmp
        char blx_bin[13];
        if (cal_blx(*patch_addr, blx_bin) == -1) {
            return -1;
        }
        blx_bin[12] = '\x00';


        if (write_kernel_text(insn->insn_addr, blx_bin, 12, PERM_W) == -1) {
            printf("write kernel text failed\n");
            return -1;
        }

        *patch_addr = (*patch_addr) + insn->patch_size + PATCH_GAP;
        insn = insn->next;
    }

    return 0;
}
