#!/bin/bash

pos=`grep -P -a -b -m 1 --only-matching '\x1F\x8B\x08\x00' $1 | cut -f 1 -d :`
echo "Extracting gzip'd kernel image from file: $1 (start = $pos)"

if [ ! -z $pos ]; then
echo "Dumping compressed image"
dd if=$1 of=$2 bs=1 skip=$pos 2>/dev/null >/dev/null
echo "Unzipping compressed image"
gunzip -qf $2
fi
