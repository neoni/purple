#ifndef PATCH_H
#define PATCH_H

#include <stdio.h>
#include "func.h"


#define MAX_PATCH_CODE_LEN    300
#define PATCH_GAP             60

#define PATCH_CODE_END        "\x0e\xf0\xa0\xe1"     // mov pc, lr
#define NOP                   "\x00\x00\xa0\xe1"    // mov r0, r0

typedef struct _PatchInsn {
    size_t insn_addr;
    size_t patch_size;
    char patch_code[MAX_PATCH_CODE_LEN];
    struct _PatchInsn *next;
} PatchInsn;


typedef struct _Patch {
    char name[31];
    PatchInsn *patchinsn;
    Func *func;
} Patch;


void init_patch(Patch *patch, char* name);
void fini_patch(Patch *patch);
void add_patchinsn(Patch *patch, size_t addr, size_t size, char *code);
int nop_insn(size_t addr, size_t insn_num);
int start_patch(Patch *patch, size_t *patch_addr);

#endif
