#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <capstone.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "asm.h"

int disasm(Func *func) {

    csh handle;
    cs_insn *insn;
    size_t count;

    if (cs_open(CS_ARCH_ARM, CS_MODE_ARM + CS_MODE_LITTLE_ENDIAN, &handle) != CS_ERR_OK) {
        printf("ERROR: Failed to disassemble func %s!\n", func->name);
        return -1;
    }

    cs_option(handle, CS_OPT_DETAIL, CS_OPT_ON);
    count = cs_disasm(handle, (uint8_t *)func->code, func->size, func->addr, 0, &insn);

#ifdef DEBUG
    printf("func %s has %d instructions.\n", func->name, count);
#endif

    if (count > 0) {
#ifdef DETAIL
        size_t j;
        for (j = 0; j < count; j++) {
            printf("0x%"PRIx64":\t%s\t\t%s\n", insn[j].address, insn[j].mnemonic, insn[j].op_str);
        }
#endif
        func->insn_count = count;
        func->insn = insn;
        //cs_free(insn, count);
    } else {
        printf("ERROR: Failed to disassemble func %s!\n", func->name);
        return -1;
    }
    func->handle = handle;
    //cs_close(&handle);

    return 0;
}


int asmble(char *code, char *bin, size_t *size, char *filename) {

#ifndef KEYSTONE
    FILE *fp;

    if ((fp = fopen(filename, "w")) == NULL) {
        perror("ERROR: Failed to open patch_code.s\n");
        return -1;
    }

    fwrite(code, 1, strlen(code), fp);
    fclose(fp);

    char cmd[100];
    snprintf(cmd, 100, "./as -o patch_code.o %s", filename);
    system(cmd);
    system("./objcopy -O binary patch_code.o patch_bin");

    if ((fp = fopen("./patch_bin", "rb")) == NULL) {
        perror("ERROR: Failed to open patch_bin\n");
        return -1;
    }

    struct stat info;
    if (stat("patch_bin", &info) != 0) {
        printf("stat failed\n");
        return -1;
    }

    *size = info.st_size;
#ifdef DEBUG
    printf("asm size is %d\n", *size);
#endif

    fread(bin, 1, *size, fp);
    fclose(fp);
#else
    ks_engine *ks;
    ks_err err;
    size_t count;
    unsigned char *encode;

    err = ks_open(KS_ARCH_ARM, KS_MODE_32, &ks);
    if (err != KS_ERR_OK) {
        printf("ERROR: failed on ks_open(), quit\n");
        return -1;
    }
    if (ks_asm(ks, code, 0, &encode, &size, &count) != KS_ERR_OK) {
        printf("ERROR: ks_asm() failed & count = %lu, error = %u\n", count, ks_errno(ks));
    } else {
        size_t i;
        printf("%s = ", code);
        for (i = 0; i < size; i++) {
            printf("%02x ", encode[i]);
        }
        printf("\n");
        printf("Compiled: %lu bytes, statements: %lu\n", size, count);

    }
    ks_free(encode);
    ks_close(ks);
#endif
    return 0;
}


void cal_bl(size_t src, size_t dst, size_t *bin) {

    *bin = 0xeb000000 + (((dst - src) / 4 - 2) & 0xffffff);

#ifdef DEBUG
    printf("cal bl addr src(%x) -> dst(%x) is %x\n", src, dst, *bin);
#endif
}


int cal_blx(size_t dst, char *bin) {

    char code[40], code_bin[40];
    size_t size;

    snprintf(code, 40, "movw r12, #%d\nmovt r12, #%d\n", dst & 0xffff, dst >> 16);
    if (asmble(code, code_bin, &size, "bl.s") == -1) {
        printf("assemble blx_code failed\n");
        return -1;
    }

    memcpy(bin, code_bin, 8);
    memcpy(bin+8, "\x3c\xff\x2f\xe1", 4);
    bin[12] = '\x00';
#ifdef DEBUG
    size_t i;
    printf("cal blx addr dst(%x) is:\n", dst);
    for (i = 0; i < 12; ++i) {
        printf("%02x", bin[i]);
    }
    printf("\n");
#endif

    return 0;
}


void cal_bne(size_t src, size_t dst, size_t *bin) {
    *bin = 0x1a000000 + (((dst - src - 8) / 4) & 0xffffff);
#ifdef DEBUG
    printf("cal bne addr src(%x) -> dst(%x) is %x\n", src, dst, *bin);
#endif
}


size_t cal_ldr(size_t src, size_t dst, size_t reg) {

    size_t off;

    off = ((dst - src - 2) >> 2) & 0xff;
    off = ((72 + reg) << 8) + off;

#ifdef DEBUG
    printf("cal ldr offset src(%x) -> dst(%x) is %x\n", src, dst, off);
#endif

    return off;
}


int extract_op_mem(cs_insn *insn, size_t *reg, size_t *imm) {
    if (!strncmp(insn->mnemonic, "ldr", 3)) {
        *reg = insn->detail->arm.operands[1].mem.base;
        *imm = insn->detail->arm.operands[1].mem.disp;
    } else if (!strncmp(insn->mnemonic, "add", 3)) {
        *reg = insn->detail->arm.operands[1].reg;
        *imm = insn->detail->arm.operands[2].imm;
    } else if (!strncmp(insn->mnemonic, "sub", 3)) {
        *reg = insn->detail->arm.operands[1].reg;
        *imm = insn->detail->arm.operands[2].imm;
    } else {
        printf("extract insn reg and imm failed\n");
        return -1;
    }
    return 0;
}
