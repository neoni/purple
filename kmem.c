#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <linux/types.h>

#include "kmem.h"

/* read data from kmem */
int read_kmem(size_t offset, void *buf, size_t count) {
    int fd;
    int n = 0;

    fd = open("/dev/kmem", O_RDONLY);
    if (fd < 0) {
        perror("open /dev/kmem failed\n");
        return -1;
    }

    if (lseek(fd, offset, SEEK_SET) != offset) {
        perror("/dev/kmem lseek failed\n");
        close(fd);
        return -1;
    }

    while (n < count) {
        int res = read(fd, buf+n, count-n);
        if (res < 0) {
#ifndef BLAST
            perror("/dev/kmem read failed\n");
#endif
            close(fd);
            return -1;
        }
        n += res;
    }

    close(fd);
    return n;
}


int read_kmem_func(Func *func) {
    return read_kmem(func->addr, func->code, func->size);
}


/* write data to kmem */
int write_kmem(size_t offset, void *buf, size_t count) {
    int fd;
    int n = 0;

    fd = open("/dev/kmem", O_WRONLY);
    if (fd < 0) {
        perror("open /dev/kmem failed");
        return -1;
    }

    if (lseek(fd, offset, SEEK_SET) != offset) {
        perror("/dev/kmem lseek failed\n");
        close(fd);
        return -1;
    }

    while (n < count) {
        int res = write(fd, buf+n, count-n);
        if (res < 0) {
            perror("/dev/kmem write failed\n");
            close(fd);
            return -1;
        }
        n += res;
    }

    close(fd);
    return n;
}




