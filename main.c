#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <linux/slab.h>
#include <sys/mman.h>

#include "cve.h"
#include "func.h"
#include "utils.h"
#include "patch.h"
#include "locate.h"

size_t patch_addr;

static int patch_model(char *cvename, char funcname[][51], size_t func_num) {

    Patch patch;
    size_t i;

    init_patch(&patch, cvename);

    Func *func = patch.func;
    for (i = 0; i < func_num; ++i) {
        Func *newfunc = malloc(sizeof(Func));
        if (funcname[i][strlen(funcname[i])-1] == '*') {
            funcname[i][strlen(funcname[i])-1] = '\x00';
            if (init_func(newfunc, funcname[i], true) == -1) {
                return -1;
            }
        } else {
            if (init_func(newfunc, funcname[i], false) == -1) {
                return -1;
            }
        }
        if (func == NULL) {
            patch.func = newfunc;
            func = newfunc;
        } else {
            func->next = newfunc;
            func = func->next;
        }
    }

    if (call_func(&patch) == -1) {
        return -1;
    }

    // patching
    if (start_patch(&patch, &patch_addr) == -1) {
        printf("patch failed\n");
        return -1;
    }

    fini_patch(&patch);

    return 0;
}


static int patch_cve_2015_3636(int version) {

    startup("cve-2015-3636");

    if (version == 4) {
        char funcname[1][51] = {"ping_v4_unhash"};
        if (patch_model("cve_2015_3636", funcname, 1) == -1) {
            return -1;
        }
    } else {
        char funcname[1][51] = {"ping_unhash"};
        if (patch_model("cve_2015_3636", funcname, 1) == -1) {
            return -1;
        }
    }

    return 0;
}


static int patch_cve_2014_3153() {

    startup("cve-2014-3153");

    char funcname[2][51] = {"futex_requeue", "futex_wait_requeue_pi.constprop*"};
    if (patch_model("cve_2014_3153", funcname, 2) == -1) {
        return -1;
    }

    return 0;
}


int main(int argc, char **argv) {

    int version;

    init_symbol_maps();
    version = get_version();

    if (version == -1) {
        printf("get version failed\n");
        return -1;
    }

    if (vmalloc_exe_kmem(&patch_addr) == -1) {
        printf("generate patch addr failed\n");
        return -1;
    }

    if (patch_cve_2015_3636(version) == -1) {
        printf("\ncve-2015-3636 patching failed\n\n");
    } else {
        printf("\ncve-2015-3636 patching succeeded\n\n");
    }

    if (patch_cve_2014_3153() == -1) {
        printf("\ncve-2014-3153 patching failed\n\n");
    } else {
        printf("\ncve-2014-3153 patching succeeded\n\n");
    }

    return 0;
}


