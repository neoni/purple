#ifndef LOCATE_H
#define LOCATE_H

#include "func.h"

void init_symbol_maps();
int locate_by_symbol(Func *func, bool fuzz);
int get_symbol_addr(char *name, size_t *addr);
int locate_by_xref(Func *xref_func, Func *func);


#endif
