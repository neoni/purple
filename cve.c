#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <capstone.h>

#include "cve.h"
#include "asm.h"
#include "func.h"
#include "patch.h"
#include "locate.h"

int locate_cve_2015_3636(Patch *patch) {

    size_t reg = -1;
    size_t pass = 0;
    size_t i;
    cs_insn *patch_insn = NULL;
    Func *func = patch->func;

    for (i = 0; i < func->insn_count; ++i) {
        cs_insn insn = func->insn[i];
        if (insn.detail == NULL) {
            continue;
        }
        if (reg != -1) {
            pass++;
            if (pass >= 30) {
                break;
            }
        }
        cs_arm *arm = &(insn.detail->arm);
        if (reg == -1 && !strncmp(insn.mnemonic, "mov", 3) && arm->op_count == 2 &&
            arm->operands[0].type == ARM_OP_REG && arm->operands[1].type == ARM_OP_REG &&
            !strncmp(cs_reg_name(func->handle, arm->operands[1].reg), "r0", 2)) {
            reg = arm->operands[0].reg;
#ifdef DEBUG
            printf("0x%" PRIx64 ":\t%s\t%s\n", insn.address, insn.mnemonic, insn.op_str);
#endif
        }
        if ((reg != -1) && !strncmp(insn.mnemonic, "str", 3) && arm->op_count == 2 &&
            arm->operands[1].type == ARM_OP_MEM && arm->operands[1].mem.base != ARM_REG_INVALID &&
            arm->operands[1].mem.base == reg) {
#ifdef DEBUG
            printf("0x%" PRIx64 ":\t%s\t%s\n", insn.address, insn.mnemonic, insn.op_str);
#endif
            patch_insn = &insn;
            break;
        }
    }

    // obtain patch address
    if (patch_insn == NULL) {
        printf("can't find patching point\n");
        return -1;
    }

    char patch_code[100];
    char patch_bin[100];
    size_t patch_size;

    snprintf(patch_code, 100, "mov %s, #0\n", cs_reg_name(func->handle,
             patch_insn->detail->arm.operands[0].reg));

#ifdef DEBUG
    printf("patch_code is\n%s\n", patch_code);
#endif

    if (asmble(patch_code, patch_bin, &patch_size, "patch.s") == -1) {
        printf("assemble patch_code failed\n");
        return -1;
    }
#ifdef DEBUG
    for (i = 0; i < patch_size; ++i) {
        printf("%02x", patch_bin[i]);
    }
    printf("\n");
#endif

#ifdef TEST
    #include "locate.h"

    size_t dump_stack_addr;
    get_symbol_addr("dump_stack", &dump_stack_addr);
    memcpy(patch_bin, "\xfe\x5f\x2d\xe9", 4);
    cal_blx(dump_stack_addr, patch_bin+4);
    memcpy(patch_bin+16, "\xfe\x5f\x9d\xe8", 4);
    patch_bin[20] = '\x00';
    patch_size = 20;
#endif

    add_patchinsn(patch, patch_insn->address, patch_size, patch_bin);

    return 0;
}


int locate_cve_2014_3153(Patch *patch) {

    cs_insn *patch_insn = NULL;
    char patch_code[300];
    char patch_bin[300];
    char new_bin[300];
    size_t patch_size;
    size_t i, j;

    Func* func = patch->func;

    // first insn
    patch_insn = &func->insn[2];
#ifdef DEBUG
            printf("0x%" PRIx64 ":\t%s\t%s\n", patch_insn->address, patch_insn->mnemonic, patch_insn->op_str);
#endif

    snprintf(patch_code, 200, "cmp r0, r2\nmvn r0, #0x15\n");

#ifdef DEBUG
    printf("patch_code is\n%s\n", patch_code);
#endif

    if (asmble(patch_code, new_bin, &patch_size, "patch.s") == -1) {
        printf("assemble patch_code failed\n");
        return -1;
    }

    char blx_bin[12];
    size_t blx_addr = 0;
    for (i = func->insn_count - 1; i > func->insn_count - 10; --i) {
        if ((strncmp(func->insn[i].mnemonic, "ldm", 3) == 0) || (strncmp(func->insn[i].mnemonic, "pop", 3) == 0)) {
            blx_addr = func->insn[i-1].address;
            break;
        }
    }
    if (blx_addr == 0) {
        printf("can't find ldm addr\n");
        return -1;
    }

    cal_blx(blx_addr, blx_bin);

    memcpy(patch_bin, new_bin, 4);
    memcpy(patch_bin+4, "\x03\x00\x00\x1a", 4);   // bne
    memcpy(patch_bin+8, new_bin+4, 4);
    memcpy(patch_bin+12, blx_bin, 12);

    patch_size = 24;
    patch_bin[patch_size] = '\x00';

#ifdef DEBUG
    for (i = 0; i < patch_size; ++i) {
        printf("%02x", patch_bin[i]);
    }
    printf("\n");
#endif
    add_patchinsn(patch, patch_insn->address, patch_size, patch_bin);


    // second insn
    memset(patch_code, 0, sizeof(patch_code));
    memset(patch_bin, 0, sizeof(patch_bin));
    memset(new_bin, 0, sizeof(new_bin));
    patch_size = 0;
    patch_insn = NULL;

    size_t hash_futex_addr = 0, get_user_addr = 0;
    if (get_symbol_addr("hash_futex", &hash_futex_addr) == -1) {
        printf("find second insn failed\n");
        return -1;
    }
    if (get_symbol_addr("__get_user_4", &get_user_addr) == -1) {
        printf("find second insn failed\n");
        return -1;
    }


    size_t out_put_key_addr = 0, reg = -1;
    cs_insn *key1 = NULL;
    cs_insn *key2 = NULL;
    cs_insn *requeue_pi = NULL;
    for (i = 0; i < func->insn_count; ++i) {
        if (reg != -1) {
            break;
        }

        cs_insn insn = func->insn[i];
        if (insn.detail == NULL) {
            continue;
        }
        cs_arm *arm = &(insn.detail->arm);

        // find requeue_pi
        if ((requeue_pi == NULL) && (!strncmp(insn.mnemonic, "beq", 3) || !strncmp(insn.mnemonic, "bne", 3))) {
            // backtracking
            for (j = i-1; j > i-10; --j) {
                if (!strncmp(func->insn[j].mnemonic, "ldr", 3)) {
                    requeue_pi = &func->insn[j];
#ifdef DEBUG
                    printf("requeue_pi: 0x%" PRIx64 ":\t%s\t%s\n", requeue_pi->address, requeue_pi->mnemonic, requeue_pi->op_str);
#endif
                    break;
                }
            }
            if (requeue_pi == NULL) {
                printf("can't find requeue_pi\n");
                return -1;
            }
        }

        // find key1 & key2, patch_insn
        if (!key1 && !strncmp(insn.mnemonic, "b", 1) && arm->operands[0].imm == hash_futex_addr) {
            // find key1
            for (j = i-1; j > i-5; --j) {
                if (!strncmp(cs_reg_name(func->handle, func->insn[j].detail->arm.operands[0].reg), "r0", 2)) {
                    key1 = &func->insn[j];
                    patch_insn = &func->insn[j];
#ifdef DEBUG
                    printf("key1: 0x%" PRIx64 ":\t%s\t%s\n", key1->address, key1->mnemonic, key1->op_str);
#endif
                    break;
                }
            }
            // find key2
            for (j = i+1; j < i+5; ++j) {
                if (!strncmp(cs_reg_name(func->handle, func->insn[j].detail->arm.operands[0].reg), "r0", 2)) {
                    key2 = &func->insn[j];
#ifdef DEBUG
                    printf("key2: 0x%" PRIx64 ":\t%s\t%s\n", key2->address, key2->mnemonic, key2->op_str);
#endif
                    break;
                }
            }
        }

        // find out_put_key
        if (!out_put_key_addr && !strncmp(insn.mnemonic, "b", 1) && arm->operands[0].imm == get_user_addr) {
            for (j = i+1; j < i+10; ++j) {
                if (!strncmp(func->insn[j].mnemonic, "bne", 3)) {
                    out_put_key_addr = func->insn[j].detail->arm.operands[0].imm;
#ifdef DEBUG
                    printf("out_put_key: 0x%" PRIx64 ":\t%s\t%s\n", func->insn[j].address, func->insn[j].mnemonic, func->insn[j].op_str);
#endif
                    break;
                }
            }
        }

        // find reg
        if (out_put_key_addr && insn.address == out_put_key_addr) {
            for (j = i; j < i+5; ++j) {
                if (!strncmp(func->insn[j].mnemonic, "mov", 3) && func->insn[j].detail->arm.op_count == 2 &&
                    func->insn[j].detail->arm.operands[0].type == ARM_OP_REG && func->insn[j].detail->arm.operands[1].type == ARM_OP_REG) {
                    reg = func->insn[j].detail->arm.operands[1].reg;
#ifdef DEBUG
                    printf("reg: 0x%" PRIx64 ":\t%s\t%s\n", func->insn[j].address, func->insn[j].mnemonic, func->insn[j].op_str);
#endif
                    break;
                }
            }
        }
    }

    if (patch_insn == NULL || requeue_pi == NULL || key1 == NULL || key2 == NULL || reg == -1) {
        printf("can't find second patching point\n");
        return -1;
    }

    size_t key1_reg, key1_imm, key2_reg, key2_imm;
    if ((extract_op_mem(key1, &key1_reg, &key1_imm) == -1) || (extract_op_mem(key2, &key2_reg, &key2_imm) == -1)) {
        return -1;
    }

    snprintf(patch_code, 300, "%s %s\ncmp %s, #0\nldr r0, [%s, #0x%x]\nldr r1, [%s, #0x%x]\ncmp r0, r1\n\
ldr r0, [%s, #0x%x]\nldr r1, [%s, #0x%x]\ncmp r0, r1\nldr r0, [%s, #0x%x]\nldr r1, [%s, #0x%x]\ncmp r0, r1\n\
mvn %s, #0x15\n%s %s\nstmfd sp!, {lr}\nldmfd sp!, {lr}\n\n",
             requeue_pi->mnemonic, requeue_pi->op_str,
             cs_reg_name(func->handle, requeue_pi->detail->arm.operands[0].reg),
             cs_reg_name(func->handle, key1_reg), key1_imm,
             cs_reg_name(func->handle, key2_reg), key2_imm,
             cs_reg_name(func->handle, key1_reg), strncmp(cs_reg_name(func->handle, key1_reg), "fp", 2) ? key1_imm+4 : key1_imm-4,
             cs_reg_name(func->handle, key2_reg), strncmp(cs_reg_name(func->handle, key2_reg), "fp", 2) ? key2_imm+4 : key2_imm-4,
             cs_reg_name(func->handle, key1_reg), strncmp(cs_reg_name(func->handle, key1_reg), "fp", 2) ? key1_imm+8 : key1_imm-8,
             cs_reg_name(func->handle, key2_reg), strncmp(cs_reg_name(func->handle, key2_reg), "fp", 2) ? key2_imm+8 : key2_imm-8,
             cs_reg_name(func->handle, reg),
             key1->mnemonic, key1->op_str);

#ifdef DEBUG
    printf("patch_code is\n%s\n", patch_code);
#endif

    if (asmble(patch_code, new_bin, &patch_size, "patch2.s") == -1) {
        printf("assemble patch_code failed\n");
        return -1;
    }

    memset(blx_bin, 0, sizeof(blx_bin));
    cal_blx(out_put_key_addr, blx_bin);

    memcpy(patch_bin, new_bin, 8);
    memcpy(patch_bin+8, "\x0f\x00\x00\x0a", 4);   // beq
    memcpy(patch_bin+12, new_bin+8, 12);
    memcpy(patch_bin+24, "\x0b\x00\x00\x1a", 4);  // bne
    memcpy(patch_bin+28, new_bin+20, 12);
    memcpy(patch_bin+40, "\x07\x00\x00\x1a", 4);  // bne
    memcpy(patch_bin+44, new_bin+32, 12);
    memcpy(patch_bin+56, "\x03\x00\x00\x1a", 4);  // bne
    memcpy(patch_bin+60, new_bin+44, 4);
    memcpy(patch_bin+64, blx_bin, 12);
    memcpy(patch_bin+76, new_bin+48, 8);

    memset(blx_bin, 0, sizeof(blx_bin));
    cal_blx(hash_futex_addr, blx_bin);

    memcpy(patch_bin+84, blx_bin, 12);
    memcpy(patch_bin+96, new_bin+56, 4);

    patch_size = 100;

#ifdef DEBUG
    for (i = 0; i < patch_size; ++i) {
        printf("%02x", patch_bin[i]);
    }
    printf("\n");
#endif

    add_patchinsn(patch, patch_insn->address, patch_size, patch_bin);
    nop_insn(patch_insn->address, 2);


    // third insn
    func = func->next;
    if (func == NULL) {
        printf("can't find second func\n");
        return -1;
    }

    memset(patch_code, 0, sizeof(patch_code));
    memset(patch_bin, 0, sizeof(patch_bin));
    memset(new_bin, 0, sizeof(new_bin));
    patch_size = 0;
    patch_insn = NULL;

    size_t futex_wait_setup_addr = 0, get_futex_key_addr = 0;
    if (get_symbol_addr("futex_wait_setup", &futex_wait_setup_addr) == -1) {
        printf("find third insn failed\n");
        return -1;
    }
    if (get_symbol_addr("get_futex_key", &get_futex_key_addr) == -1) {
        printf("find third insn failed\n");
        return -1;
    }


    out_put_key_addr = 0;
    reg = -1;
    key1 = NULL;
    key2 = NULL;
    size_t bl_num = 0;
    for (i = 0; i < func->insn_count; ++i) {
        if (reg != -1 && out_put_key_addr) {
            break;
        }

        cs_insn insn = func->insn[i];
        if (insn.detail == NULL) {
            continue;
        }
        cs_arm *arm = &(insn.detail->arm);

        // find key1
        if (!key1 && !strncmp(insn.mnemonic, "b", 1) && arm->operands[0].imm == get_futex_key_addr) {
            for (j = i-1; j > i-13; --j) {
                if (!strncmp(cs_reg_name(func->handle, func->insn[j].detail->arm.operands[0].reg), "r2", 2)) {
                    if (func->insn[j].detail->arm.op_count == 2 && func->insn[j].detail->arm.operands[1].type == ARM_OP_REG) {
                        size_t r = func->insn[j].detail->arm.operands[1].reg;
                        size_t k;
                        for (k = j-1; k > j-13; --k) {
                            if (func->insn[k].detail->arm.operands[0].reg == r) {
                                key1 = &func->insn[k];
                                break;
                            }
                        }
                    } else {
                        key1 = &func->insn[j];
                    }
#ifdef DEBUG
                    printf("key1(key2): 0x%" PRIx64 ":\t%s\t%s\n", key1->address, key1->mnemonic, key1->op_str);
#endif
                    break;
                }
            }
        }

        // find key2
        if (!key2 && !strncmp(insn.mnemonic, "b", 1) && arm->operands[0].imm == futex_wait_setup_addr) {
            for (j = i-1; j > i-13; --j) {
                if (!strncmp(cs_reg_name(func->handle, func->insn[j].detail->arm.operands[0].reg), "r3", 2)) {
                    key2 = &func->insn[j];
#ifdef DEBUG
                    printf("key2(q): 0x%" PRIx64 ":\t%s\t%s\n", key2->address, key2->mnemonic, key2->op_str);
#endif
                    break;
                }
            }
        }

        // find out_put_key
        if (!out_put_key_addr && key2 && !strncmp(insn.mnemonic, "bne", 3)) {
            out_put_key_addr = arm->operands[0].imm - 8;
            patch_insn = &func->insn[i+1];
#ifdef DEBUG
            printf("out_put_key: 0x%" PRIx64 ":\t%s\t%s\n", insn.address, insn.mnemonic, insn.op_str);
#endif
        }

        // find reg
        if (reg == -1 && insn.address == out_put_key_addr) {
            for (j = i+1; j < i+20; ++j) {
                if (!strncmp(func->insn[j].mnemonic, "b", 1)) {
                    ++bl_num;
                }
                if (bl_num > 3 && !strncmp(func->insn[j].mnemonic, "mov", 3) && func->insn[j].detail->arm.op_count == 2 &&
                    func->insn[j].detail->arm.operands[0].type == ARM_OP_REG && func->insn[j].detail->arm.operands[1].type == ARM_OP_REG &&
                    !strncmp(cs_reg_name(func->handle, func->insn[j].detail->arm.operands[0].reg), "r0", 2)) {
                    reg = func->insn[j].detail->arm.operands[1].reg;
#ifdef DEBUG
                    printf("reg: 0x%" PRIx64 ":\t%s\t%s\n", func->insn[j].address, func->insn[j].mnemonic, func->insn[j].op_str);
#endif
                    break;
                }
            }
        }

    }

    if (patch_insn == NULL || key1 == NULL || key2 == NULL || reg == -1) {
        printf("can't find third patching point\n");
        return -1;
    }

    if ((extract_op_mem(key1, &key1_reg, &key1_imm) == -1) || (extract_op_mem(key2, &key2_reg, &key2_imm) == -1)) {
        return -1;
    }

    snprintf(patch_code, 300, "ldr r0, [%s, #0x%x]\nldr r1, [%s, #0x%x]\ncmp r0, r1\n\
ldr r0, [%s, #0x%x]\nldr r1, [%s, #0x%x]\ncmp r0, r1\nldr r0, [%s, #0x%x]\nldr r1, [%s, #0x%x]\ncmp r0, r1\n\
mvn %s, #0x15\n\n",
             cs_reg_name(func->handle, key1_reg), key1_imm,
             cs_reg_name(func->handle, key2_reg), key2_imm,
             cs_reg_name(func->handle, key1_reg), strncmp(cs_reg_name(func->handle, key1_reg), "fp", 2) ? key1_imm+4 : key1_imm-4,
             cs_reg_name(func->handle, key2_reg), strncmp(cs_reg_name(func->handle, key2_reg), "fp", 2) ? key2_imm+4 : key2_imm-4,
             cs_reg_name(func->handle, key1_reg), strncmp(cs_reg_name(func->handle, key1_reg), "fp", 2) ? key1_imm+8 : key1_imm-8,
             cs_reg_name(func->handle, key2_reg), strncmp(cs_reg_name(func->handle, key2_reg), "fp", 2) ? key2_imm+8 : key2_imm-8,
             cs_reg_name(func->handle, reg));

#ifdef DEBUG
    printf("patch_code is\n%s\n", patch_code);
#endif

    if (asmble(patch_code, new_bin, &patch_size, "patch3.s") == -1) {
        printf("assemble patch_code failed\n");
        return -1;
    }

    memset(blx_bin, 0, sizeof(blx_bin));
    cal_blx(out_put_key_addr, blx_bin);

    memcpy(patch_bin, new_bin, 12);
    memcpy(patch_bin+12, "\x0b\x00\x00\x1a", 4);  // bne
    memcpy(patch_bin+16, new_bin+12, 12);
    memcpy(patch_bin+28, "\x07\x00\x00\x1a", 4);  // bne
    memcpy(patch_bin+32, new_bin+24, 12);
    memcpy(patch_bin+44, "\x03\x00\x00\x1a", 4);  // bne
    memcpy(patch_bin+48, new_bin+36, 4);
    memcpy(patch_bin+52, blx_bin, 12);

    patch_size = 64;


#ifdef DEBUG
    for (i = 0; i < patch_size; ++i) {
        printf("%02x", patch_bin[i]);
    }
    printf("\n");
#endif

    add_patchinsn(patch, patch_insn->address, patch_size, patch_bin);

    return 0;
}


int call_func(Patch *patch) {
    int i;

    for (i = 0; i < (sizeof(function_map) / sizeof(function_map[0])); i++) {
        if (!strcmp(function_map[i].name, patch->name) && function_map[i].func) {
            if (function_map[i].func(patch) == -1) {
                return -1;
            }
            return 0;
        }
    }

    printf("can't find  patch func\n");

    return -1;
}

